import 'dart:math';

class IMCModel {
  double imc;
  bool isNormal () => imc < 25 && imc > 18.5;
  String res () => isNormal() ? "Es normal" : "No Es normal";

  IMCModel(double altura, double peso) {
    imc = peso / (pow(altura/100,2));
  }

}